<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Jobs\testSendToRedis;

class testRedis extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'testRedis';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {   
        testSendToRedis::dispatch()->onQueue('default')->onConnection('redis1');
        return Command::SUCCESS;
    }
}
